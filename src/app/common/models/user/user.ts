export interface User {
  id: string;
  username: string;
  //roles: [{ id: string; name: string }];
  isLoggedIn: boolean;
  password?: string;
}