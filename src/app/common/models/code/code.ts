export interface Code {
  status: string;
  code: string;
  prize?: string;
}
