import { Component, OnInit, Input, ViewContainerRef} from "@angular/core";
import { FormControl, Validators } from '@angular/forms';
import { ModalDialogOptions, ModalDialogService } from "nativescript-angular";
import { RouterExtensions } from "nativescript-angular/router";
import { AuthService } from '../shared/services/auth.service';
import { Code } from '../common/models/code/code';
import { CodeService } from '../shared/services/code.service';
import { ModalComponent } from "../modal/modal.component";

@Component({
    selector: "ns-barcode",
    moduleId: module.id,
    templateUrl: "./barcode.component.html",
    styleUrls: ['./barcode.component.css']
})
export class BarcodeComponent implements OnInit {
    checkCode = new FormControl('');
    code: Code;
    _barcode = "";

    constructor(
        private vcRef: ViewContainerRef,
        private routerExtensions: RouterExtensions,
        private authService: AuthService,
        private codeService: CodeService,
        private modalService: ModalDialogService,
    ) { }

    ngOnInit(): void {
        this.checkCode.setValidators([
            Validators.required,
            Validators.minLength(13),
            Validators.maxLength(13),
        ]);
    }
    onScanResult(event) {
        console.log(`onScanResult: ${event.text} (${event.format})`);
        this._barcode = event.text;
    }
    clearValue() {
        this._barcode = '';
    }
    openModal(params: any) {
        const codeValue = this.checkCode.value;
        this.checkIfCodeIsValid(codeValue).subscribe(data => {
            let options: ModalDialogOptions = {
                viewContainerRef: this.vcRef,
                context: {
                    status: data.status,
                    value: codeValue,
                    prize: data.prize,
                },
                fullscreen: true
            };
            this.modalService.showModal(ModalComponent, options).then (
                (result) => {
                    console.log(data.status);
                }
            );
        });
    }

    logout() {
        this.authService.logout();
        this.routerExtensions.navigate(["/login"], { clearHistory: true });
    }

    checkIfCodeIsValid(code: string) {
        return this.codeService.checkIfCodeIsValid(code);
    }
}