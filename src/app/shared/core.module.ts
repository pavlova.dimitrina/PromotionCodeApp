import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AuthService } from './services/auth.service';
import { CodeService } from './services/code.service';
import { OutletService } from './services/outlet.service';
import { StorageService } from './services/storage.service';
import { UserService } from './services/user.service';

@NgModule({
  imports: [CommonModule, HttpClientModule],
  providers: [
    AuthService,
    CodeService,
    OutletService,
    StorageService,
    UserService
  ],
})
export class CoreModule {}
