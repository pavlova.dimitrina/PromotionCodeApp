import { Injectable } from '@angular/core';

import * as appSettings from 'tns-core-modules/application-settings';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  public set(key: string, value: string): void {
    appSettings.setString(key, value);
  }

  public get(key: string): string {
    return appSettings.getString(key);
  }

  public remove(key: string): void {
    appSettings.remove(key);
  }
}
