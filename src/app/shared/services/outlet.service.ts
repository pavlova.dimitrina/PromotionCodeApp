import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class OutletService {
  private devUrl = 'http://localhost:3000';

  private awsUrl =
    'http://ec2-35-180-48-128.eu-west-3.compute.amazonaws.com:3000';

  constructor(private http: HttpClient) {}

  public getAllOutlets() {
    return this.http.get<any>(`${this.awsUrl}/outlets`);
  }

  public getOutlet(id: string) {
    return this.http.get<any>(`${this.awsUrl}/outlets/${id}`);
  }
}
