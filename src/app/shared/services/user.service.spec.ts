import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { UserService } from './user.service';

describe('UserService', () => {
  const http = jasmine.createSpyObj('HttpClient', [
    'get',
    'put',
  ]);

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: http,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it('getUser() should return the user', () => {
    const service: UserService = TestBed.get(UserService);
    const username = 'testuser';

    http.get.and.returnValue(
      of({
        id: 'b7552156-1035-41ca-bbef-484d19affe94',
        username: 'testuser',
        createdOn: new Date(Date.now()),
        isLoggedIn: true,
        isDeleted: false,
        redemptionRecords: [],
        outlet: {},
      }),
    );

    service.getUser(username).subscribe(res => {
      expect(res.username).toBe('testuser');
    });
    http.get.calls.reset();
  });

  it('getAllUsers() should return an array of users', () => {
    const service: UserService = TestBed.get(UserService);
    const users = [
      {
        id: 'b7552156-1035-41ca-bbef-484d19affe94',
        username: 'testuser1',
        createdOn: new Date(Date.now()),
        isLoggedIn: true,
        redemptionRecords: [],
        outlet: {},
      },
      {
        id: 'b7552156-1035-41ca-bbef-484d19affe94',
        username: 'testuser2',
        createdOn: new Date(Date.now()),
        isLoggedIn: true,
        redemptionRecords: [],
        outlet: {},
      },
    ];

    http.get.and.returnValue(
      of([
        {
          id: 'b7552156-1035-41ca-bbef-484d19affe94',
          username: 'testuser1',
          createdOn: new Date(Date.now()),
          isLoggedIn: true,
          redemptionRecords: [],
          outlet: {},
        },
        {
          id: 'b7552156-1035-41ca-bbef-484d19affe94',
          username: 'testuser2',
          createdOn: new Date(Date.now()),
          isLoggedIn: true,
          isDeleted: false,
          redemptionRecords: [],
          outlet: {},
        },
      ]),
    );

    service.getAllUsers().subscribe(res => {
      expect(res[0].username).toBe(users[0].username);
      expect(res[1].username).toBe(users[1].username);
    });
    http.get.calls.reset();
  });

  http.put.calls.reset();
});
