import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Code } from '../../common/models/code/code';

@Injectable({
  providedIn: 'root',
})
export class CodeService {
  private devUrl = 'http://localhost:3000';

  private awsUrl =
    'http://ec2-35-180-48-128.eu-west-3.compute.amazonaws.com:3000';

  constructor(private http: HttpClient) {}

  checkIfCodeIsValid(code: string) {
    return this.http.get<any>(`${this.awsUrl}/codes/${code}`);
  }

  redeemCode(code: string) {
    return this.http.post<any>(`${this.awsUrl}/codes/`, { code });
  }

  getAllRedeemedCodes() {
    return this.http.get<any>(`${this.awsUrl}/codes`);
  }

  reportMultipleRedemptionAttempt(code: Code) {
    return this.http.post<void>(`${this.awsUrl}/reports`, code);
  }
}
