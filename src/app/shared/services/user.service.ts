import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private devUrl = 'http://localhost:3000';

  private awsUrl =
    'http://ec2-35-180-48-128.eu-west-3.compute.amazonaws.com:3000';

  constructor(private http: HttpClient) {}

  getAllUsers() {
    return this.http.get<any>(`${this.awsUrl}/users`);
  }

  getUser(id: string) {
    return this.http.get<any>(`${this.awsUrl}/users/${id}`);
  }

}
