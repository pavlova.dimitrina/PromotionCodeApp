import { TestBed } from '@angular/core/testing';

import { OutletService } from './outlet.service';
import { HttpClient } from '@angular/common/http';

describe('OutletService', () => {
  const http = jasmine.createSpyObj('HttpClient', [
    'get',
  ]);

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: http,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: OutletService = TestBed.get(OutletService);
    expect(service).toBeTruthy();
  });
});
