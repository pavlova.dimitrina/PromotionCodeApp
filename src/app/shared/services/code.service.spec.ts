import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { CodeService } from './code.service';

describe('CodeService', () => {
  const http = jasmine.createSpyObj('HttpClient', ['get', 'post']);

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: http,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: CodeService = TestBed.get(CodeService);
    expect(service).toBeTruthy();
  });
});
