import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UserLogin } from '../../common/models/user/login-user';
import { User } from '../../common/models/user/user';
import { UserSubject } from '../../common/models/user/user-subject';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private userSubject$ = new BehaviorSubject<UserSubject | null>(this.user);

  private devUrl = 'http://localhost:3000';

  private awsUrl =
    'http://ec2-35-180-48-128.eu-west-3.compute.amazonaws.com:3000';

  constructor(private http: HttpClient, private storage: StorageService) {}

  get user$() {
    return this.userSubject$.asObservable();
  }

  private get user() {
    const token = this.storage.get('token');
    const username = this.storage.get('username') || '';
    //const role = this.storage.get('role') || '';
    if (token) {
      return { username };
    }

    return null;
  }

  login(user: UserLogin) {
    return this.http.post(`${this.awsUrl}/auth/login`, user).pipe(
      tap((res: { token: string; user: User }) => {
        this.userSubject$.next({
          username: res.user.username,
          //role: res.user.roles[0].name,
        });
        this.storage.set('token', res.token);
        this.storage.set('username', res.user.username);
        //this.storage.set('role', res.user.roles[0].name);
      }),
    );
  }

  logout() {
    this.storage.remove('token');
    this.storage.remove('username');
    //this.storage.remove('role');
    this.userSubject$.next(null);
    return this.http.delete(`${this.devUrl}/auth/logout`);
  }
}
