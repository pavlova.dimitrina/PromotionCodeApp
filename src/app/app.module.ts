import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
// import { BrowserModule } from '@angular/platform-browser';
import { NativeScriptFormsModule } from "nativescript-angular/forms"
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { ReactiveFormsModule } from '@angular/forms';
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

import { TokenInterceptorService } from './common/interceptors/token-interceptor.service';
import { CoreModule } from './shared/core.module';
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { registerElement } from "nativescript-angular/element-registry";
import { BarcodeComponent } from "./barcode/barcode.component";
import { BarcodeScanner } from "nativescript-barcodescanner";
import { ModalComponent } from "./modal/modal.component";
registerElement("BarcodeScanner", () => require("nativescript-barcodescanner").BarcodeScannerView);
registerElement("PreviousNextView", () => require("nativescript-iqkeyboardmanager").PreviousNextView);


// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";

// Uncomment and add to NgModule imports if you need to use the HttpClient wrapper
// import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        CoreModule,
        NativeScriptFormsModule, 
        ReactiveFormsModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        BarcodeComponent,
        ModalComponent,
    ],
    providers: [
        BarcodeScanner,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptorService,
            multi: true,
        },
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    entryComponents: [
        ModalComponent,
    ],
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
