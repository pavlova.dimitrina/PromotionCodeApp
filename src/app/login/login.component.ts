import { Component } from "@angular/core";
import { FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms'
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";

import { UserLogin } from '../common/models/user/login-user';
import { AuthService } from '../shared/services/auth.service';

@Component({
    selector: "ns-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    loginForm: FormGroup;
    isLoggingIn:boolean = true;
    processing:boolean = false;

    constructor(
        private page: Page,  
        private routerExtensions: RouterExtensions,
        private formBuilder: FormBuilder,
        private authService: AuthService,
    ) {
        this.page.actionBarHidden = true;
    }
    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', [Validators.required, Validators.minLength(5)]],
            password: ['', [Validators.required, Validators.minLength(5)]],
        });
    }

    toggleForm() {
        this.isLoggingIn = !this.isLoggingIn;
    }

    login() {
        const user: UserLogin = this.loginForm.value;
        this.authService.login(user).subscribe(
          () => {
            this.processing = true;
            this.routerExtensions.navigate(["/barcode"], { clearHistory: true });
          },
          (error) => {
            // console.log(error)
            dialogs.alert({
                title: "Login failed",
                message: "Please check your username and password",
                okButtonText: "Close" }).then();
            },
        );
    }
}