import { Component, OnInit } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular";
import { Code } from '../common/models/code/code';
import { CodeService } from '../shared/services/code.service';
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
  selector: "modal",
  moduleId: module.id,
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.css"],

})

export class ModalComponent implements OnInit {

  constructor(
    private params: ModalDialogParams,
    private codeService: CodeService,
  ) {}

  codeInfo: Code;
  public parans:any;

  ngOnInit() {
    this.parans = this.params.context;
    console.log("parans");
    console.log(this.parans);
  }

  close() {
    this.params.closeCallback();
  }

  reportMultipleRedemptionAttempt() {
    return this.codeService
      .reportMultipleRedemptionAttempt(this.codeInfo)
      .subscribe(
        () => {
          dialogs.alert({
            message: "Your code was reported",
            okButtonText: "Ok" }).then();
          setTimeout( () => this.close(), 3000);
        },
        () => {
          dialogs.alert({
            message: "Sorry, something went wrong",
            okButtonText: "Ok" }).then();
          setTimeout( () => this.close(), 3000);
        },
      );
  }

  redeemCode(codeValue: string) {
    return this.codeService.redeemCode(codeValue).subscribe(
      () => {
        dialogs.alert({
          message: "Code has been marked as redeemed.",
          okButtonText: "Ok" }).then();
        setTimeout( () => this.close(), 3000);
      },
      () => {
        dialogs.alert({
          message: "Sorry, something went wrong.",
          okButtonText: "Ok" }).then();
        setTimeout( () => this.close(), 3000);
      },
    );
  }
}