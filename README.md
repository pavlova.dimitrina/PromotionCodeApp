# Client

This project was generated with Nativescript CLI: npm install -g nativescript.

## Development server

Run `tns run ios --emulator` for a dev server. Navigate to Xcode emulator. The app will automatically reload if you change any of the source files.

Run `tns run ios` for a dev server. Navigate to Xcode emulator. Upload app oh tne telephone.

## Running unit tests

Run `tns test ios` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `tns e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Nativescript CLI use `tns help` or go check out the [Nativescrip CLI](https://docs.nativescript.org/angular/start/quick-setup).
